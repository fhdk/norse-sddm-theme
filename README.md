## Norse
Simple SDDM theme

The original is found at [https://gitlab.com/isseigx/simplicity-sddm-theme][1]

![Screenshot](./Norse/norse.png)


### Install
Move **Norse** folder to

    /usr/share/sddm/themes

edit file

    /etc/sddm.conf

search for

    [Theme]
    Current=theme-name

and change to

    [Theme]
    Current=Norse

#### ArchLinux
From [AUR](https://aur.archlinux.org/packages/simplicity-sddm-theme-git/)

#### Font
Recommended font: Noto Sans

If you use SDDM version 0.16 or above please read [this](https://github.com/sddm/sddm/wiki/0.16.0-Release-Announcement#configuration)

To change theme graphically and more use [SDDM Config Editor](https://github.com/lxqt/sddm-config-editor)

[1]: https://gitlab.com/isseigx/simplicity-sddm-theme
